﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebMyCare.Models
{
    public class WebDbContext : DbContext
    {
        public DbSet<User> UserProfiles { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Kind> Kinds { get; set; }
        public DbSet<Rating> Rates { get; set; }
        public DbSet<Advertise> Advertises { get; set; }
    }
}