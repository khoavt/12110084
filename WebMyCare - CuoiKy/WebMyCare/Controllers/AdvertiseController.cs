﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMyCare.Models;

namespace WebMyCare.Controllers
{
    public class AdvertiseController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Advertise/

        public ActionResult Index()
        {
            var advertises = db.Advertises.Include(a => a.Kind);
            return View(advertises.ToList());
        }

        //
        // GET: /Advertise/Details/5

        public ActionResult Details(int id = 0)
        {
            Advertise advertise = db.Advertises.Find(id);
            if (advertise == null)
            {
                return HttpNotFound();
            }
            return View(advertise);
        }

        //
        // GET: /Advertise/Create

        public ActionResult Create()
        {
            ViewBag.KindId = new SelectList(db.Kinds, "KindId", "Name");
            return View();
        }

        //
        // POST: /Advertise/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Advertise advertise)
        {
            if (ModelState.IsValid)
            {
                db.Advertises.Add(advertise);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KindId = new SelectList(db.Kinds, "KindId", "Name", advertise.KindId);
            return View(advertise);
        }

        //
        // GET: /Advertise/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Advertise advertise = db.Advertises.Find(id);
            if (advertise == null)
            {
                return HttpNotFound();
            }
            ViewBag.KindId = new SelectList(db.Kinds, "KindId", "Name", advertise.KindId);
            return View(advertise);
        }

        //
        // POST: /Advertise/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Advertise advertise)
        {
            if (ModelState.IsValid)
            {
                db.Entry(advertise).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KindId = new SelectList(db.Kinds, "KindId", "Name", advertise.KindId);
            return View(advertise);
        }

        //
        // GET: /Advertise/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Advertise advertise = db.Advertises.Find(id);
            if (advertise == null)
            {
                return HttpNotFound();
            }
            return View(advertise);
        }

        //
        // POST: /Advertise/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Advertise advertise = db.Advertises.Find(id);
            db.Advertises.Remove(advertise);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}