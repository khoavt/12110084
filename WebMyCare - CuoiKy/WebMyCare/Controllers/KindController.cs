﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMyCare.Models;

namespace WebMyCare.Controllers
{
    public class KindController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Kind/

        public ActionResult Index()
        {
            return View(db.Kinds.ToList());
        }

        //
        // GET: /Kind/Details/5

        public ActionResult Details(int id = 0)
        {
            Kind kind = db.Kinds.Find(id);
            if (kind == null)
            {
                return HttpNotFound();
            }
            return View(kind);
        }

        //
        // GET: /Kind/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Kind/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Kind kind)
        {
            if (ModelState.IsValid)
            {
                db.Kinds.Add(kind);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kind);
        }

        //
        // GET: /Kind/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Kind kind = db.Kinds.Find(id);
            if (kind == null)
            {
                return HttpNotFound();
            }
            return View(kind);
        }

        //
        // POST: /Kind/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Kind kind)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kind).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kind);
        }

        //
        // GET: /Kind/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Kind kind = db.Kinds.Find(id);
            if (kind == null)
            {
                return HttpNotFound();
            }
            return View(kind);
        }

        //
        // POST: /Kind/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kind kind = db.Kinds.Find(id);
            db.Kinds.Remove(kind);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}