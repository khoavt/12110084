﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMyCare.Models;

namespace WebMyCare.Controllers
{
    public class RatingController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Rating/

        public ActionResult Index()
        {
            var rates = db.Rates.Include(r => r.News);
            return View(rates.ToList());
        }

        //
        // GET: /Rating/Details/5

        public ActionResult Details(int id = 0)
        {
            Rating rating = db.Rates.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            return View(rating);
        }

        //
        // GET: /Rating/Create

        public ActionResult Create()
        {
            ViewBag.NewsId = new SelectList(db.News, "NewsId", "Title");
            return View();
        }

        //
        // POST: /Rating/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rating rating)
        {
            if (ModelState.IsValid)
            {
                db.Rates.Add(rating);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NewsId = new SelectList(db.News, "NewsId", "Title", rating.NewsId);
            return View(rating);
        }

        //
        // GET: /Rating/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rating rating = db.Rates.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            ViewBag.NewsId = new SelectList(db.News, "NewsId", "Title", rating.NewsId);
            return View(rating);
        }

        //
        // POST: /Rating/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rating rating)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rating).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NewsId = new SelectList(db.News, "NewsId", "Title", rating.NewsId);
            return View(rating);
        }

        //
        // GET: /Rating/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rating rating = db.Rates.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            return View(rating);
        }

        //
        // POST: /Rating/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rating rating = db.Rates.Find(id);
            db.Rates.Remove(rating);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}