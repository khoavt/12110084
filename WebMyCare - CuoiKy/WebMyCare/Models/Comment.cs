﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMyCare.Models
{
    public class Comment
    {
        public int CommentId { set; get; }
        public DateTime Date { set; get; }
        public string Content { set; get; }
        public string Username { set; get; }

        public int NewsId { set; get; }
        public virtual News News { set; get; }
    }
}