﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMyCare.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string password { get; set; }
        public string Gender { set; get; }
        public string Job { get; set; }
        public string email { get; set; }
        public string phone { set; get; }
        public virtual ICollection<News> Newses { set; get; }
    }
}