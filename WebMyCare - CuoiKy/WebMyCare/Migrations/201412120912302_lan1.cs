namespace WebMyCare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        password = c.String(),
                        Gender = c.String(),
                        Job = c.String(),
                        email = c.String(),
                        phone = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        NewsId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Date = c.DateTime(nullable: false),
                        Content = c.String(),
                        NumberofView = c.String(),
                        status = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        KindId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NewsId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Kinds", t => t.KindId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.KindId);
            
            CreateTable(
                "dbo.Kinds",
                c => new
                    {
                        KindId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.KindId);
            
            CreateTable(
                "dbo.Advertises",
                c => new
                    {
                        AdvertiseId = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Url = c.String(),
                        UrlHinh = c.String(),
                        KindId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdvertiseId)
                .ForeignKey("dbo.Kinds", t => t.KindId, cascadeDelete: true)
                .Index(t => t.KindId);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        RatingId = c.Int(nullable: false, identity: true),
                        Decription = c.String(),
                        RatingTimes = c.String(),
                        NewsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RatingId)
                .ForeignKey("dbo.News", t => t.NewsId, cascadeDelete: true)
                .Index(t => t.NewsId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Content = c.String(),
                        Username = c.String(),
                        NewsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.News", t => t.NewsId, cascadeDelete: true)
                .Index(t => t.NewsId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "NewsId" });
            DropIndex("dbo.Ratings", new[] { "NewsId" });
            DropIndex("dbo.Advertises", new[] { "KindId" });
            DropIndex("dbo.News", new[] { "KindId" });
            DropIndex("dbo.News", new[] { "UserId" });
            DropForeignKey("dbo.Comments", "NewsId", "dbo.News");
            DropForeignKey("dbo.Ratings", "NewsId", "dbo.News");
            DropForeignKey("dbo.Advertises", "KindId", "dbo.Kinds");
            DropForeignKey("dbo.News", "KindId", "dbo.Kinds");
            DropForeignKey("dbo.News", "UserId", "dbo.Users");
            DropTable("dbo.Comments");
            DropTable("dbo.Ratings");
            DropTable("dbo.Advertises");
            DropTable("dbo.Kinds");
            DropTable("dbo.News");
            DropTable("dbo.Users");
        }
    }
}
