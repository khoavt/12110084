﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMyCare.Models
{
    public class Rating
    {
        public int RatingId { set; get; }
        public string Decription { set; get; }
        public string RatingTimes { set; get; }

        public int NewsId { set; get; }
        public virtual News News { set; get; }
    }
}