﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMyCare.Models
{
    public class News
    {
        public int NewsId { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public DateTime Date { set; get; }
        public string Content { set; get; }
        public string NumberofView { set; get; }
        public Boolean status { set; get; }

        public int UserId { set; get; }
        public int KindId { set; get; }
        public virtual User User { set; get; }
        public virtual Kind Kind { set; get; }
        public virtual ICollection<Rating> Rates { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
    }
}