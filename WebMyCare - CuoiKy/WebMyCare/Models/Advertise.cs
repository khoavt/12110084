﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMyCare.Models
{
    public class Advertise
    {
        public int AdvertiseId { set; get; }
        public string Description { set; get; }
        public string Url { set; get; }
        public string UrlHinh { set; get; }

        public int KindId { set; get; }
        public virtual Kind Kind { set; get; }
    }
}