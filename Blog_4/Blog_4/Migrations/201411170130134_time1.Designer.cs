// <auto-generated />
namespace Blog_4.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class time1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(time1));
        
        string IMigrationMetadata.Id
        {
            get { return "201411170130134_time1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
