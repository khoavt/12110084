﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMyCare.Models
{
    public class Kind
    {
        public int KindId { set; get; }
        public string Name { set; get; }
        public Boolean Status { set; get; }
        public virtual ICollection<News> Newses { set; get; }
        public virtual ICollection<Advertise> Advertises { set; get; }
    }
}